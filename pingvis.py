# trapping sigint
import signal
import sys

import os
import subprocess
import time
import datetime
import json
from shutil import which
import threading

import re

class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKCYAN = '\033[96m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

pythonVersionMajor = sys.version_info[0];
pythonVersionSub = sys.version_info[1];
if(pythonVersionMajor < 3):
	print("Only python3 is supported\n")
	exit(-1)

#-#-#-#-# SETTINGS #-#-#-#-#-#

settingsFile = open("config.json")
settings = json.load(settingsFile)
settingsFile.close()

# Parse json

# Radio IPs to be pinged
if(not "ipList" in settings):
	print("IP list not found from settings file! Add 'ipList' to config.json")
	exit(1)
ipList = settings['ipList']
if(not ipList):
	print("IP list in settings file is empty!")
	exit(1)

# If true, make a beeping alarm whenever any of the set IPs is down
beepOnDisconnect = True # default
if("beepOnDisconnect" in settings):
	beepOnDisconnect = settings['beepOnDisconnect']

alarmAdudioFile = "BEEP.wav"
if("alarmAudioFile" in settings):
	alarmAdudioFile = settings['alarmAudioFile']

statusSymbols = {"connected": "⬤", "disconnected": "⬤" }
if("statusSymbols" in settings):
	statusSymbols = settings['statusSymbols']

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#


# List containing the status symbols for each IP
ipStatuses = {}
ipRespondtimes = {}


#-#-#-#-#-# HELPER FUNCTIONS #-#-#-#-#-#

#Check whether `name` is on PATH and marked as executable
def toolExists(name):
	return which(name) is not None


shouldRun = True

# Asynchronic Pinger
def pingIP(ip):
	global shouldRun
	while shouldRun:
		try:
			response = subprocess.check_output(
				['ping', '-c', '1', '-w', '3', ip],
				stderr=subprocess.STDOUT,  # get all output
				universal_newlines=True  # return string not bytes
			)
		except subprocess.CalledProcessError:
			response = None

		# Assign status symbol for all
		if response == None:
			ipStatuses[ip] =  "disconnected"
		else:
			respondTime = re.search("(?<=time=)\d.\.*\d*", response)
			ipStatuses[ip] =  "connected"
			if type(respondTime) != type(None):
				ipRespondtimes[ip] = respondTime[0]



def playAlarm(): 
	# Alarm limit is for timing the beeping
	alarmRate = 2 # For easier change, do every x seconds
	alarmTimer = datetime.datetime.now()  
	global shouldRun
	while shouldRun:
		# Play sound with selected player and wait until it's done
		if(beepOnDisconnect): # Only do if set to beep (on unless disabled in settings)
			if( alarmTimer < datetime.datetime.now() - datetime.timedelta(seconds = alarmRate) ):
				alarmTimer = datetime.datetime.now()
				if( "disconnected" in ipStatuses.values() ):
					with subprocess.Popen(
						[audioPlayer, alarmAdudioFile],
						stdout=subprocess.DEVNULL,
						stderr=subprocess.STDOUT
					) as p:
						try:
							p.wait(timeout=1)
						except subprocess.TimeoutExpired:
							assert p.returncode is None


# Print the table
def printter(ipList):
	# Print is limited because clear causes it to flash
	printRate = 0.5 # For easier change, do every x seconds
	printTimer = datetime.datetime.now()
	global shouldRun
	while shouldRun:
		if( printTimer < datetime.datetime.now() - datetime.timedelta(seconds = printRate) ):
			printTimer = datetime.datetime.now()
			os.system('clear')

			for ip in ipList:
				if(ip not in ipStatuses or ipStatuses[ip] == "disconnected"):
					print(ip + f":{bcolors.FAIL}\t", statusSymbols['disconnected'], f"{bcolors.ENDC}")
				else:				
					ipRespondMessage = ""
					if ip in ipRespondtimes:
						ipRespondMessage = "(" + ipRespondtimes[ip] + " ms)"			
					print(ip + f":{bcolors.OKGREEN}\t", statusSymbols['connected'], ipRespondMessage, f"{bcolors.ENDC}")


# Sigint handler for ctrl+c
def signal_handler(sig, frame):
	print('\nUser interrupted. Exiting')
	global shouldRun
	shouldRun = False
	try:
		for thread in threads:
			thread.join()
		sys.exit(0)
	except:
		pass

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#


# Select correct audio player based on the audio server
# The program should come with the installed server package
audioPlayer = "aplay" # ALSA
if( not toolExists(audioPlayer) ):
	audioPlayer = "paplay" # Pipewire
if( not toolExists(audioPlayer) ):
	print("No audio software installed. Alarm disabled")
	beepOnDisconnect = False # Disable beeps as there's no audio

# Register sigint trap for ctrl-c
signal.signal(signal.SIGINT, signal_handler)


#-#-#-#-#-# MAIN #-#-#-#-#-#

threads = []

for ip in ipList:
	thread = threading.Thread( target=pingIP, args=(ip,) )
	thread.start()
	threads.append(thread)

printThread = threading.Thread( target=printter, args=(ipList,) )
printThread.start()
threads.append(printThread)

alarmThread = threading.Thread( target=playAlarm )
alarmThread.start()
threads.append(alarmThread)

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
