## Ping Visualizer
Simple ping interface made in Python. Shows statuses of IPs set in config file and beeps if any of them is disconnected.
![ping](ping.png)



## Dependencies
- Python 3
- ALSA or Pipewire
- Which ever package `ping` comes with


<!-- USAGE EXAMPLES -->
## Usage
1. Clone the repo
2. Change the config file to include the IPs you want to ping and the other settings if you want
4. Give the .py file execution permission with `sudo chmod a+x pingvis.py` if needed
3. inside the repo directory, run `python ./pingvis.py`
